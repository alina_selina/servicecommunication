﻿using Shared;
using System;

namespace Pinger
{
    public class Program
    {
        public static void Main()
        {
            Console.WriteLine("Pinger started [press any key to exit]");

            var broker = new Broker(Settings.Ponger, Settings.Pinger);
            var pinger = new Pinger(broker);
            pinger.Start();

            Console.ReadKey();
        }
    }
}
