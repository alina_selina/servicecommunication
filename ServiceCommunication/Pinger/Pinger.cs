﻿using Shared;

namespace Pinger
{
    public class Pinger
    {
        private readonly Broker _broker;

        public Pinger(Broker broker)
        {
            _broker = broker;
        }

        public void Start()
        {
            _broker.SendMessageToQueue();
            _broker.ListenQueue();
        }
    }
}
