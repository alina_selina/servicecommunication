﻿using Shared;
using System;

namespace Ponger
{
    public class Program
    {
        public static void Main()
        {
            Console.WriteLine("Ponger started [press any key to exit]");

            var broker = new Broker(Settings.Pinger, Settings.Ponger);
            var ponger = new Ponger(broker);
            ponger.Start();

            Console.ReadKey();
        }
    }
}
