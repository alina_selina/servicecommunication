﻿using Shared;

namespace Ponger
{
    public class Ponger
    {
        private readonly Broker _broker;

        public Ponger(Broker broker)
        {
            _broker = broker;
        }

        public void Start()
        {
            _broker.ListenQueue();
        }
    }
}
