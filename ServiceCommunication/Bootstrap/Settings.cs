﻿using System;
using System.IO;
using System.Reflection;

namespace Bootstrap
{
    public static class Settings
    {
        public const ConsoleColor ErrorMessageColor = ConsoleColor.Red;

        public const string Instruction = "Первым должен стартовать Ponger и слушать свою очередь, далее стартует Pinger и отправляет сообщение + слушает свою очередь.";

        public static readonly string ResoursesPath = 
            $@"{Directory.GetParent(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)).Parent.Parent}{Path.DirectorySeparatorChar}Resourses{Path.DirectorySeparatorChar}";
    }
}
