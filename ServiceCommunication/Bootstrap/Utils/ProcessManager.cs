﻿using System.Diagnostics;

namespace Bootstrap.Utils
{
    public static class ProcessManager
    {
        public static Process CreateProcess(string fileName)
        {
            var command = new Process();
            var commandInfo = new ProcessStartInfo
            {
                CreateNoWindow = false,
                UseShellExecute = true,
                FileName = fileName
            };

            command.StartInfo = commandInfo;
            return command;
        }

        public static void StartProcess(string fileName, out Process command)
        {
            command = CreateProcess(fileName);
            command.Start();
        }
    }
}
