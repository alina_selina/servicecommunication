﻿using System;
using static System.Console;

namespace Bootstrap
{
    public static class ConsoleUtils
    {
        public static void WaitForAnyKeyPress(string message = "\nPress any key to continue...")
        {
            WriteLine(message);
            ReadKey();
        }

        public static void WriteErrorMessage(string message, bool newLine = false)
        {
            WriteMessage(message, Settings.ErrorMessageColor, newLine);
        }

        public static void WriteMessage(string message, ConsoleColor color, bool newLine = false)
        {
            ForegroundColor = color;
            if (newLine)
                WriteLine(message);
            else
                Write(message);
            ResetColor();
        }
    }
}
