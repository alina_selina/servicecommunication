﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using static Bootstrap.Utils.ProcessManager;

namespace Bootstrap
{
    class Program
    {
        private static Dictionary<int, Operation> _mainMenu;
        private static string _header = "PING-PONG";
        private static List<Process> _processes;

        static Program()
        {
            _processes = new();
            InitializeMenu();
        }

        static void Main()
        {
            StartMenu(_mainMenu, _header);
        }

        static void StartMenu(Dictionary<int, Operation> menu, string header)
        {
            while (true)
            {
                Console.Clear();
                Menu.ShowHeader(header);
                Console.WriteLine($"{Settings.Instruction}\n");
                Menu.Show(menu);
                Menu.SelectOperation(menu);
            }
        }

        static void InitializeMenu()
        {
            _mainMenu = new Dictionary<int, Operation>
            {
                {1, new Operation("Add Ponger", new Action(()=> {
                    StartProcess($"{Settings.ResoursesPath}Ponger.exe", out Process p);
                    _processes.Add(p);
                }))
                },
                {2, new Operation("Add Pinger", new Action(()=> {
                    StartProcess($"{Settings.ResoursesPath}Pinger.exe", out Process p);
                    _processes.Add(p);
                }))
                },
                {3, new Operation("Quit", new Action(()=> {
                    try
                    {
                        _processes.ForEach(p => p.Kill());
                    }
                    finally
                    {
                        Environment.Exit(0);
                    }
                }))
                }
            };
        }
    }
}
