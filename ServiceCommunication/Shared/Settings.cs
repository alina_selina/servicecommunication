﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;
using System;
using System.Collections.Generic;

namespace Shared
{
    public static class Settings
    {
        public static readonly Uri RabbitMQUri = new("amqp://guest:guest@localhost:5672/");

        public static readonly MessageScopeSettings Pinger = new()
        {
            ExchangeName = "Exchange",
            ExhangeType = ExchangeType.Direct,
            QueueName = "ping_queue",
            RoutingKey = "ping"
        };

        public static readonly MessageScopeSettings Ponger = new()
        {
            ExchangeName = "Exchange",
            ExhangeType = ExchangeType.Direct,
            QueueName = "pong_queue",
            RoutingKey = "pong"
        };

        public static readonly Dictionary<MessageScopeSettings, string> Messages = new()
        {
            { Pinger, "Ping" },
            { Ponger, "Pong" }
        };
    }
}
