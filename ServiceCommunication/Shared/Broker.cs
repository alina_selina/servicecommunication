﻿using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Services;

namespace Shared
{
    public class Broker
    {
        public QueueService QueueService { get; }
        public MessageScopeSettings Producer { get; }
        public MessageScopeSettings Consumer { get; }

        public Broker(MessageScopeSettings producer, MessageScopeSettings consumer)
        {
            var connection = new ConnectionFactory(Settings.RabbitMQUri).Connection;
            IMessageProducerScopeFactory messageProducerScopeFactory = new MessageProducerScopeFactory(connection);
            IMessageConsumerScopeFactory messageConsumerScopeFactory = new MessageConsumerScopeFactory(connection);

            QueueService = new QueueService(messageProducerScopeFactory, messageConsumerScopeFactory, producer, consumer);
            Producer = producer;
            Consumer = consumer;
        }

        public void SendMessageToQueue()
        {
            QueueService.SendMessageToQueue(Settings.Messages[Consumer]);
        }

        public void ListenQueue()
        {
            QueueService.ListenQueue(() => QueueService.SendMessageToQueue(Settings.Messages[Consumer]));
        }
    }
}