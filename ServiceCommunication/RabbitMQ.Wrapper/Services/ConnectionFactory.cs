﻿using RabbitMQ.Client;
using System;

namespace RabbitMQ.Wrapper.Services
{
    public class ConnectionFactory
    {
        public Client.ConnectionFactory Connection { get; }

        public ConnectionFactory(Uri uri, int timeout = 30, int recoveryInterval = 30, bool autoRecoveryEnabled = true,
                                 bool topologyRecoveryEnabled = true, int requestedHeartbeat = 60)
        {
            Connection = new Client.ConnectionFactory
            {
                Uri = uri ?? throw new ArgumentException("incorrect Uri"),
                RequestedConnectionTimeout = TimeSpan.FromSeconds(timeout),
                NetworkRecoveryInterval = TimeSpan.FromSeconds(recoveryInterval),
                AutomaticRecoveryEnabled = autoRecoveryEnabled,
                TopologyRecoveryEnabled = topologyRecoveryEnabled,
                RequestedHeartbeat = TimeSpan.FromSeconds(requestedHeartbeat)
            };
        }

        public IConnection CreateConnection() => Connection.CreateConnection();
    }
}
