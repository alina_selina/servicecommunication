﻿using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;
using System.Text;
using System.Threading;

namespace RabbitMQ.Wrapper.Services
{
    public class QueueService : IQueueSender, IQueueListener, IDisposable

    {
        private readonly IMessageProducerScope _messageProducerScope;
        private readonly IMessageConsumerScope _messageConsumerScope;

        public QueueService(IMessageProducerScopeFactory messageProducerScopeFactory, IMessageConsumerScopeFactory messageConsumerScopeFactory,
            MessageScopeSettings producer, MessageScopeSettings consumer)
        {
            _messageProducerScope = messageProducerScopeFactory.Open(producer);
            _messageConsumerScope = messageConsumerScopeFactory.Connect(consumer);
        }

        public void ListenQueue(Action sendMessage)
        {
            _messageConsumerScope.MessageConsumer.Received += (sender, args) =>
            {
                GetMessage(args);
                Thread.Sleep(2500);
                sendMessage.Invoke();
            };
        }

        public bool SendMessageToQueue(string message)
        {
            try
            {
                _messageProducerScope.MessageProducer.Send(message);
                Console.WriteLine($"[{DateTime.Now}]: {message} sent");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Dispose()
        {
            _messageProducerScope?.Dispose();
            _messageConsumerScope?.Dispose();
        }

        #region Helpers
        private void GetMessage(Client.Events.BasicDeliverEventArgs args)
        {
            var message = Encoding.UTF8.GetString(args.Body.ToArray());
            Console.WriteLine($"[{DateTime.Now}]: {message} received");

            _messageConsumerScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, true);
        }
        #endregion
    }
}