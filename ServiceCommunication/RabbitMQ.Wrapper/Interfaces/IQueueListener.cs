﻿using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IQueueListener
    {
        void ListenQueue(Action sendMessage = null);
    }
}
