﻿using RabbitMQ.Client;
using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageQueue : IDisposable
    {
        IModel Channel { get; }
        void DeclareExchange(string exchangeName, string exchangeType);
        void BindQueue(string exhangeName, string routingKey, string queueName);
    }
}
