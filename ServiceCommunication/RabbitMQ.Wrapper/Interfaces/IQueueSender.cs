﻿namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IQueueSender
    {
        bool SendMessageToQueue(string message);
    }
}
